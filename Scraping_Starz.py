import requests
import json
import os

#API
url = 'https://playdata.starz.com/metadata-service/play/partner/Web_AR/v8/blocks?playContents=map&lang=es-419&pages=BROWSE,HOME,MOVIES,PLAYLIST,SEARCH,SEARCH%20RESULTS,SERIES&includes=contentId,contentType,title,product,seriesName,seasonNumber,free,comingSoon,newContent,topContentId,properCaseTitle,categoryKeys,runtime,popularity,original,firstEpisodeRuntime,releaseYear,images,minReleaseYear,maxReleaseYear,episodeCount,detail'

#Respuesta de la API
response = requests.get(url)

#Respuesta de la API en JSON
json_response = response.json()

#Validación de respuesta con el sitio web
if response.status_code == 200:

    #Peliculas y series
    movies_and_series = []

    #Información series
    info_series = []

    #Información peliculas
    info_movies = []

    for data in json_response['blocks']:

        if data['blockType'] == 'PLAY_CONTENTS':

            for info in data['playContentsById']:
                print("\n" + data['playContentsById'][info]['contentType'] + ": " + data['playContentsById'][info]['title'] + "\n")

                movies_and_series.append(str(data['playContentsById'][info]['contentType'] + ": " + data['playContentsById'][info]['title']))

                #Título, año, sinopsis, link y duración(solo para movies)
                if data['playContentsById'][info]['contentType'] == 'Series with Season':
                    print(str("Titulo: " + data['playContentsById'][info]['title']))
                    print(str("Sinopsis: " + data['playContentsById'][info]['detail']))
                    print(str("Año: " + data['playContentsById'][info]['maxReleaseYear']))

                    info_series.append(str("Titulo: " + data['playContentsById'][info]['title']))
                    info_series.append(str("Sinopsis: " + data['playContentsById'][info]['detail']))
                    info_series.append((str("Anio: " + data['playContentsById'][info]['maxReleaseYear'])))

                    #Falta url

                elif (data['playContentsById'][info]['contentType'] == 'Movie'):
                    print(str("Titulo: " + data['playContentsById'][info]['title']))
                    print(str("Sinopsis: " + data['playContentsById'][info]['detail']))
                    print(str("Año: " + data['playContentsById'][info]['releaseYear']))

                    info_movies.append(str("Titulo: " + data['playContentsById'][info]['title']))
                    info_movies.append(str("Sinopsis: " + data['playContentsById'][info]['detail']))
                    info_movies.append(str("Anio: " + data['playContentsById'][info]['releaseYear']))

                    ##Falta duración -- print("Duración: " + str(data['playContentsById'][info]['runtime']))
                    ##Falta url

else:
    print("\n¡Error, no hemos podido encontrar el sitio web!")

#Genera el JSON
movies_and_series_json = json.dumps(movies_and_series, indent=4, sort_keys=True)
info_series_json = json.dumps(info_series, indent=4, sort_keys=True)
info_movies_json = json.dumps(info_movies, indent=4, sort_keys=True)

#Escribe y guarda el JSON
with open('Scraping_movies_and_series.json', 'w') as file:
    json.dump(movies_and_series_json, file)

with open('Scraping_info_series.json', 'w') as file:
    json.dump(info_series_json, file)

with open('Scraping_info_movies.json', 'w') as file:
    json.dump(info_movies_json, file)


